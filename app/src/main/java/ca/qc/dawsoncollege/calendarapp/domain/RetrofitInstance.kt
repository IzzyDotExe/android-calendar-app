package ca.qc.dawsoncollege.calendarapp.domain

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitInstance {

    private const val BASE_URL = "https://date.nager.at/api/v3/"

    private fun getInstance() : Retrofit {
       return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    public fun getApi(): HolidayService {
        return getInstance().create(HolidayService::class.java)
    }

}