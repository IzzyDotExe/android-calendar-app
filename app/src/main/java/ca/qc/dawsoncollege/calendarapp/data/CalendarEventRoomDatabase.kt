package ca.qc.dawsoncollege.calendarapp.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import ca.qc.dawsoncollege.calendarapp.domain.CalendarEvent

@Database(entities = [(CalendarEvent::class)], version = 1)
abstract class CalendarEventRoomDatabase: RoomDatabase() {
    abstract fun calendarEventDao(): CalendarEventDao
    companion object {
        private var INSTANCE: CalendarEventRoomDatabase? = null

        fun getInstance(context: Context): CalendarEventRoomDatabase {
            synchronized(this) {
                var instance = INSTANCE
                if (instance == null) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        CalendarEventRoomDatabase::class.java,
                        "event_database"
                    ).fallbackToDestructiveMigration()
                        .build()

                    INSTANCE = instance
                }
                return instance
            }
        }
    }
}
