package ca.qc.dawsoncollege.calendarapp.data

import java.util.Date

data class HolidayResponse (
    val date: Date,
    val localName: String,
    val name: String,
    val countryCode: String,
    val fixed: Boolean,
    val global: Boolean,
    val counties: List<String>,
    val launchYear: Int,
    val types: List<String>
)