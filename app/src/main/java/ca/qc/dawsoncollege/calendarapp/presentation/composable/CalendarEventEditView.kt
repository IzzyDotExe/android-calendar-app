package ca.qc.dawsoncollege.calendarapp.presentation.composable

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import android.os.Build
import android.text.format.DateFormat
import android.widget.DatePicker
import androidx.annotation.RequiresApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import ca.qc.dawsoncollege.calendarapp.R
import ca.qc.dawsoncollege.calendarapp.domain.CalendarEvent
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import ca.qc.dawsoncollege.calendarapp.presentation.viewmodel.CalendarViewModel

@RequiresApi(Build.VERSION_CODES.O)
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CalendarEventEditView(
    event: CalendarEvent,
    navBack: () -> Unit,
    viewModel: CalendarViewModel,
    context: Context
) {
    var newEvent = false

    if (event.title == stringResource(R.string.new_event)) {
        newEvent = true
    }

    val uiState = viewModel.uiState.collectAsState()

    val startDate = java.util.Date(event.startTimestamp?.times(1000L) ?: 0)
    val endDate = java.util.Date(event.endTimestamp?.times(1000L) ?: 0)

    val eventStr : String = stringResource(R.string.event_)

    var selectedTitle by rememberSaveable {
        mutableStateOf(eventStr + event.eventId)
    }

    var selectedLocation by rememberSaveable {
        mutableStateOf(event.location)
    }

    var selectedDescription by rememberSaveable {
        mutableStateOf(event.description)
    }

    var startYear = startDate.year+1900
    var startMonth = startDate.month
    var startDay = startDate.date

    var endYear = endDate.year+1900
    var endMonth = endDate.month
    var endDay = endDate.date

    if (newEvent) {
        startYear = uiState.value.currentYear
        startMonth = uiState.value.currentMonth-1
        startDay = uiState.value.currentDay
        endYear = uiState.value.currentYear
        endMonth = uiState.value.currentMonth-1
        endDay = uiState.value.currentDay
    }

    val dateFormatter: DateTimeFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM)

    var selectedTime = remember {
        mutableStateListOf<Int>(
            startYear, startMonth,
            startDay, startDate.hours,
            startDate.minutes, startDate.seconds
        )
    }

    var selectedEndTime = remember {
        mutableStateListOf<Int>(
            endYear, endMonth,
            endDay, endDate.hours,
            endDate.minutes, endDate.seconds
        )
    }

    val datePicker = DatePickerDialog(
        context,
        { _: DatePicker, selectedYear: Int, selectedMonth: Int, selectedDayOfMonth: Int ->
            selectedTime[0] = selectedYear
            selectedTime[1] = selectedMonth
            selectedTime[2] = selectedDayOfMonth
        }, selectedTime[0], selectedTime[1], selectedTime[2]
    )

    val endDatePicker = DatePickerDialog(
        context,
        { _: DatePicker, selectedYear: Int, selectedMonth: Int, selectedDayOfMonth: Int ->
            selectedEndTime[0] = selectedYear
            selectedEndTime[1] = selectedMonth
            selectedEndTime[2] = selectedDayOfMonth
        }, selectedEndTime[0], selectedEndTime[1], selectedEndTime[2]
    )

    val timePicker = TimePickerDialog(
        context,
        { _, selectedHour: Int, selectedMinute: Int ->
            selectedTime[3] = selectedHour
            selectedTime[4] = selectedMinute
            selectedTime[5] = 0
        }, selectedTime[3], selectedTime[4], DateFormat.is24HourFormat(context)
    )

    val endTimePicker = TimePickerDialog(
        context,
        { _, selectedHour: Int, selectedMinute: Int ->
            selectedEndTime[3] = selectedHour
            selectedEndTime[4] = selectedMinute
            selectedEndTime[5] = 0
        }, selectedEndTime[3], selectedEndTime[4], DateFormat.is24HourFormat(context)
    )

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .verticalScroll(rememberScrollState()),
        horizontalAlignment = Alignment.CenterHorizontally,

    ) {

        Text(text = stringResource(id = R.string.edit_title))
        TextField(value = selectedTitle, onValueChange = {
            selectedTitle = it
        })

        Spacer(modifier = Modifier.height(10.dp))

        Text(text = stringResource(id = R.string.edit_desc))
        TextField(
            value = selectedDescription,
            onValueChange = {
                selectedDescription = it
            },
            modifier = Modifier
                .background(
                    MaterialTheme.colorScheme.primaryContainer
                )
                .padding(
                    10.dp
                )
        )
        Spacer(modifier = Modifier.height(10.dp))

        Row(
            horizontalArrangement = Arrangement.SpaceBetween,
            modifier = Modifier.fillMaxWidth()
        ) {
            TimeText()
        }

        DateSelect(
            timePicker,
            datePicker,
            selectedTime,
            dateFormatter,
            endTimePicker,
            endDatePicker,
            selectedEndTime
        )

        Text(text = stringResource(id = R.string.edit_loc))
        TextField(
            value = selectedLocation,
            onValueChange = {
                selectedLocation = it
            }
        )

        Row(
            horizontalArrangement = Arrangement.SpaceEvenly,
            modifier = Modifier
                .fillMaxWidth()
                .padding(10.dp)
        ) {
            Button(
                onClick = {
                    onEventEditClick (
                        viewModel,
                        selectedTitle,
                        selectedTime,
                        selectedEndTime,
                        selectedLocation,
                        selectedDescription,
                        event
                    )
                    navBack()
                },
                modifier = Modifier.width(120.dp)
            ) {
                Text(text = stringResource(R.string.save))
            }

        }
    }
}

@Composable
private fun DateSelect(
    timePicker: TimePickerDialog,
    datePicker: DatePickerDialog,
    selectedTime: SnapshotStateList<Int>,
    dateFormatter: DateTimeFormatter,
    endTimePicker: TimePickerDialog,
    endDatePicker: DatePickerDialog,
    selectedEndTime: SnapshotStateList<Int>
) {
    Row(
        horizontalArrangement = Arrangement.SpaceBetween,
        modifier = Modifier.fillMaxWidth()
    ) {
        Spacer(modifier = Modifier.width(0.5.dp))

        Button(onClick = {
            timePicker.show()
            datePicker.show()
        }) {
            Text(
                text = "${
                    LocalDate.of(selectedTime[0], selectedTime[1] + 1, selectedTime[2])
                        .format(dateFormatter)
                } ${"%02d".format(selectedTime[3])}:${"%02d".format(selectedTime[4])}",
                fontWeight = FontWeight.Bold,
                fontSize = 13.sp
            )
        }

        Button(onClick = {
            endTimePicker.show()
            endDatePicker.show()
        }) {
            Text(
                text = "${
                    LocalDate.of(
                        selectedEndTime[0],
                        selectedEndTime[1] + 1,
                        selectedEndTime[2]
                    ).format(dateFormatter)
                } ${"%02d".format(selectedEndTime[3])}:${"%02d".format(selectedEndTime[4])}",
                fontWeight = FontWeight.Bold,
                fontSize = 13.sp
            )
        }

        Spacer(modifier = Modifier.width(0.5.dp))
    }
}

@Composable
private fun TimeText() {
    Row {
        Spacer(modifier = Modifier.width(30.dp))
        Text(text = stringResource(R.string.start))
    }
    Row {
        Text(text = stringResource(R.string.end))
        Spacer(modifier = Modifier.width(30.dp))
    }
}

fun onEventEditClick(
    viewModel: CalendarViewModel,
    selectedTitle: String,
    selectedTime: List<Int>,
    selectedEndTime: List<Int>,
    selectedLocation: String,
    selectedDescription: String,
    event: CalendarEvent) {
    viewModel.editCurrentEvent(
        event.copy(
            title = selectedTitle,
            startTimestamp = viewModel.getEpoch(
                selectedTime[0],
                selectedTime[1],
                selectedTime[2],
                selectedTime[3],
                selectedTime[4],
                selectedTime[5]
            ),
            endTimestamp = viewModel.getEpoch(
                selectedEndTime[0],
                selectedEndTime[1],
                selectedEndTime[2],
                selectedEndTime[3],
                selectedEndTime[4],
                selectedEndTime[5]
            ),
            location = selectedLocation,
            description = selectedDescription
        )
    )
}