package ca.qc.dawsoncollege.calendarapp.presentation.viewmodel

import android.app.Activity
import android.content.Context
import android.util.Log
import androidx.compose.ui.platform.LocalContext
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import ca.qc.dawsoncollege.calendarapp.data.HolidayRepository
import ca.qc.dawsoncollege.calendarapp.domain.CalendarEvent
import kotlinx.coroutines.launch

class HolidayViewModel (activity: Activity): ViewModel() {
    private val repository = HolidayRepository()
    private val _holidays = MutableLiveData<List<CalendarEvent>>()
    val holidays : LiveData<List<CalendarEvent>> = _holidays
    private val _activity = activity;
    fun fetchHolidays(year: Int, context: Context) {
        var vm = this;
        viewModelScope.launch {
            try {
                val hols = repository.getHolidays(year, repository.getLocaleCode(context, _activity))
                var id = year + 991
                _holidays.value = hols.map {resp ->
                    id++
                    CalendarEvent(id, resp.localName, location = "", startTimestamp = (resp.date.time/1000).toInt())
                }
            } catch (e: Exception) {
                Log.e("FetchedHolidays", e.message.toString())
            }

        }
    }
}