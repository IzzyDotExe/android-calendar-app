package ca.qc.dawsoncollege.calendarapp.presentation.viewmodel

import android.app.Activity
import android.app.Application
import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import ca.qc.dawsoncollege.calendarapp.data.CalendarEventRepository
import ca.qc.dawsoncollege.calendarapp.data.CalendarEventRoomDatabase
import ca.qc.dawsoncollege.calendarapp.data.CalendarWeatherForecast
import ca.qc.dawsoncollege.calendarapp.data.TempStorage
import ca.qc.dawsoncollege.calendarapp.domain.CalendarEvent
import ca.qc.dawsoncollege.calendarapp.domain.CalendarUiState
import ca.qc.dawsoncollege.calendarapp.domain.WeatherData
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import java.util.Calendar
import java.util.GregorianCalendar
import kotlin.math.floor

class CalendarViewModel(application: Application) : ViewModel() {
    private val context = application.baseContext
    private val filename = "ForecastData.txt"
    private val _uiState = MutableStateFlow(CalendarUiState())
    val uiState: StateFlow<CalendarUiState> = _uiState.asStateFlow()
    private val _forecastDataState = MutableStateFlow(WeatherData())
    val forecastDataState: StateFlow<WeatherData> = _forecastDataState.asStateFlow()
    private var _holidayViewModel: HolidayViewModel? = null
    private val repository: CalendarEventRepository
    init {
        val eventDb = CalendarEventRoomDatabase.getInstance(application)
        val eventDao = eventDb.calendarEventDao()
        repository = CalendarEventRepository(eventDao)
        repository.allEvents.observeForever {eventList ->

            // Add unreachable first event
            if (eventList.isEmpty()) {
                repository.insertEvent(CalendarEvent(startTimestamp = 1999999999, endTimestamp = 2000000000))
                return@observeForever
            }

            _uiState.update {state ->
                state.copy(events = eventList.toMutableList())
            }
        }
    }

    fun openMap(context: Context, query: String) {
        val gmmIntentUri = Uri.parse("geo:0,0?q=$query")
        val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
        context.startActivity(mapIntent)
    }

    fun setHolidayViewModel(viewModel: HolidayViewModel) {
        this._holidayViewModel = viewModel
    }
    fun editCurrentEvent(event : CalendarEvent) {

        var newState = _uiState.value.copy()
        val events = newState.events
        val currentEvent = getCurrentEvent()
        val currentEventIndex = events.indexOf(currentEvent)

        events[currentEventIndex] = event
        repository.updateEvent(event)

        newState = newState.copy(
            events = events
        )

        _uiState.update {
            newState
        }
    }


    fun deleteEvent(event: CalendarEvent) {

        var newState = _uiState.value.copy()
        val events = newState.events
        events.remove(event)
        repository.deleteEvent(event.eventId.toString())

        newState = newState.copy(
            events = events
        )

        _uiState.update {
            newState
        }
    }

    fun getNextEventId() : Int {
        return (_uiState.value.events.maxByOrNull {
            it.eventId
        }?.eventId ?: 0) + 1
    }

    fun newEvent() : Int {
        val newEvent = CalendarEvent(eventId = getNextEventId(), startTimestamp = getEpoch(
            _uiState.value.currentYear,
            _uiState.value.currentMonth-1,
            _uiState.value.currentDay,
            13,
            0,
            0
        ),
            endTimestamp = getEpoch(
                _uiState.value.currentYear,
                _uiState.value.currentMonth-1,
                _uiState.value.currentDay,
                14,
                0,
                0
            ),  )
        val events = _uiState.value.copy().events

        events.add(newEvent)
        repository.insertEvent(newEvent)

        _uiState.update { state ->
            state.copy(events=events)
        }

        return newEvent.eventId
    }

    fun getCurrentEvent() : CalendarEvent {

        return _uiState.value.events.filter {
            it.eventId == _uiState.value.currentEvent
        }[0]

    }

    fun setCurrentEvent(eventId: Int) {
        _uiState.update { state ->
            state.copy(currentEvent = eventId)
        }
    }

    fun getDaysInMonth(x: Int): Int {
        return (28 + (x + floor(x.toDouble()/8)) % 2 + 2 % x + 2 * floor(1/x.toDouble())).toInt()
    }

    fun getEpoch(year:Int, month: Int, day: Int, hour: Int, minute: Int, second: Int) : Int {
        var cal = GregorianCalendar(
            year,
            month,
            day,
            hour,
            minute,
            second
        )
        return (cal.time.time/1000).toInt()
    }

    fun getEventsStartingOnCurrentDay(): List<CalendarEvent> {
        return _uiState.value.events.filter { event ->
            val eventStartDate = getDateFromTimestamp(event.startTimestamp)
            eventStartDate[Calendar.DAY_OF_MONTH] == _uiState.value.currentDay &&
                    eventStartDate[Calendar.MONTH] == _uiState.value.currentMonth-1 &&
                    eventStartDate[Calendar.YEAR] == _uiState.value.currentYear
        }
    }

    fun getEventsForCurrentMonth(): List<CalendarEvent> {
        return _uiState.value.events.filter { event ->
            val eventStartDate = getDateFromTimestamp(event.startTimestamp)
            eventStartDate[Calendar.DAY_OF_MONTH] >= 1 &&
                    eventStartDate[Calendar.MONTH] == _uiState.value.currentMonth-1 &&
                    eventStartDate[Calendar.YEAR] == _uiState.value.currentYear
        }
    }

    fun getDateFromTimestamp(timestamp: Int): Calendar {
        val calendar = GregorianCalendar.getInstance()
        calendar.timeInMillis = timestamp.toLong() * 1000 // Convert seconds to milliseconds
        return calendar
    }

    fun previousDay() {
        val previousDay = _uiState.value.currentDay - 1
        updateDateIfValid(previousDay)
    }

    fun nextDay() {
        val nextDay = _uiState.value.currentDay + 1
        updateDateIfValid(nextDay)
    }

    fun updateCurrentDate(year: Int, month: Int, day: Int){
        if (_uiState.value.currentYear != year) {
            _holidayViewModel?.fetchHolidays(year, context)
        }
        _uiState.update { uiState ->
            uiState.copy(
                currentYear = year,
                currentMonth = month,
                currentDay = day
            )
        }
    }


    private fun updateDateIfValid(newDay: Int) {
        val daysInMonth = getDaysInMonth(_uiState.value.currentMonth)
        var newMonth = _uiState.value.currentMonth
        var newYear = _uiState.value.currentYear
        var adjustedDay = newDay


        if (newDay in 1..daysInMonth) {
            // If the new day is within the valid range for the current month
            updateCurrentDate(_uiState.value.currentYear, _uiState.value.currentMonth, newDay)
        } else {
            // If the new day is outside the range, adjust month and year accordingly
            if (newDay < 1)
            {
                if (_uiState.value.currentMonth == 1){
                    newYear -=1
                    newMonth = 12
                }
                else { newMonth -= 1}
                adjustedDay = getDaysInMonth(newMonth)-1
            }
            else if (newDay > daysInMonth) {
                if (_uiState.value.currentMonth == 12)
                {
                    newYear += 1
                    newMonth = 1
                }else{newMonth += 1}
                adjustedDay = 1
            }
            updateCurrentDate(newYear,newMonth, adjustedDay)
        }
    }
    private fun updateDate(newDay: Int, newMonth: Int, newYear: Int) {
        _uiState.update { state ->
            state.copy(
                currentDay = newDay,
                currentMonth = newMonth,
                currentYear = newYear
            )
        }
    }
    private fun getData(activity: Activity) {
        viewModelScope.launch(Dispatchers.IO){
            CalendarWeatherForecast(context, activity).fetchData(filename)
        }
    }
    fun fetchDataAtIntervals(activity: Activity){
        viewModelScope.launch {
            while (isActive) {
                getData(activity)
                getDataFromFile()
                delay(600000)
            }
        }
    }

    private fun getDataFromFile() {
        viewModelScope.launch(Dispatchers.Main) {
            val result = TempStorage(context).readDataFromFile(filename)
            var gson = Gson()
            val jsonString = gson.fromJson(result, WeatherData::class.java)

            _forecastDataState.value = jsonString
        }
    }
}

