package ca.qc.dawsoncollege.calendarapp.domain

data class LatLong (
    var latitude: Double = 45.5017,
    var longitude: Double = -73.5673
    )
