package ca.qc.dawsoncollege.calendarapp.presentation.composable

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import ca.qc.dawsoncollege.calendarapp.domain.ForecastDay
import ca.qc.dawsoncollege.calendarapp.presentation.viewmodel.CalendarViewModel
import java.time.Instant
import java.time.ZoneId


@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun CalendarForecastView(viewModel: CalendarViewModel) {
    val forecastData = viewModel.forecastDataState.collectAsState()
    val uiState = viewModel.uiState.collectAsState()
    val currentEpoch = viewModel.getEpoch(
        uiState.value.currentYear,
        uiState.value.currentMonth-1,
        uiState.value.currentDay,
        0,0,0
    )

    val currentDateTime = Instant.ofEpochSecond(currentEpoch.toLong())
        .atZone(ZoneId.systemDefault())
        .toLocalDate()
        .atStartOfDay(ZoneId.of("GMT"))
        .toInstant()
    val forecast = forecastData.value.forecast.forecastday.filter {
        val forecastDateTime = Instant.ofEpochSecond(it.dateEpoch).atZone(ZoneId.of("GMT")).toInstant()
        forecastDateTime >=  currentDateTime
    }

    LazyColumn {
        item {
            forecast.forEach {
                DayForecast(it, viewModel)
            }
        }
    }
}

@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun DayForecast(forecast: ForecastDay,viewModel: CalendarViewModel) {
    Row {
        Column(
            Modifier.fillMaxWidth()
        ) {

            val uiState = viewModel.uiState.collectAsState()
            val currentEpoch = viewModel.getEpoch(
                uiState.value.currentYear,
                uiState.value.currentMonth-1,
                uiState.value.currentDay,
                0,0,0
            )

            Text(text = "${forecast.date}: average temp ${forecast.day.avgTempC}°C ${forecast.day.minTempC}-${forecast.day.maxTempC}")
            for (i in 1..forecast.hour.size step 3){
                val hourlyForecast = forecast.hour[i]
                Text(text = "${hourlyForecast.time}")
                Text(text = "${hourlyForecast.condition.text}")
                Text(text = "${hourlyForecast.heatindexC}°C")
            }
        }

    }
}