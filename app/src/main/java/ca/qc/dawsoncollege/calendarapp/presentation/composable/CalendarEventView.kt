package ca.qc.dawsoncollege.calendarapp.presentation.composable

import android.content.Context
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import ca.qc.dawsoncollege.calendarapp.R
import ca.qc.dawsoncollege.calendarapp.domain.CalendarEvent
import java.time.LocalDateTime
import java.time.OffsetDateTime
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import ca.qc.dawsoncollege.calendarapp.presentation.viewmodel.CalendarViewModel

@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun CalendarEventView(
    event: CalendarEvent,
    navOut: () -> Unit,
    viewModel: CalendarViewModel,
    context: Context,
    onEditClick: () -> Unit
) {

    Column(
        modifier = Modifier.fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {

        event.title?.let { Text(text = it, fontSize = 30.sp, textAlign = TextAlign.Center) }

        Spacer(modifier = Modifier.height(10.dp))

        Text(text = if (event.description == "") stringResource(R.string.no_description) else event.description,
        modifier = Modifier
            .background(
                MaterialTheme.colorScheme.primaryContainer
            )
            .padding(
                10.dp
            )
        )
        Spacer(modifier = Modifier.height(10.dp))

        DateDisplay(event = event)

        ButtonsDisplay(viewModel, context, event, onEditClick, navOut)

    }

}

@Composable
private fun ButtonsDisplay(
    viewModel: CalendarViewModel,
    context: Context,
    event: CalendarEvent,
    onEditClick: () -> Unit,
    navOut: () -> Unit
) {
    Row(
        horizontalArrangement = Arrangement.SpaceEvenly,
        modifier = Modifier
            .fillMaxWidth()
            .padding(10.dp)
    ) {

        Button(
            onClick = { viewModel.openMap(context, event.location.toString()) },
            modifier = Modifier.width(120.dp)
        ) {
            Text(text = stringResource(R.string.directions))
        }

        Button(
            onClick = { onEditClick() },
            modifier = Modifier.width(120.dp)
        ) {
            Text(text = stringResource(R.string.edit))
        }

        Button(onClick = {
            navOut()
            viewModel.setCurrentEvent(1)
            viewModel.deleteEvent(event)
        }) {
            Text(text = stringResource(R.string.delete))
        }

    }
}

@RequiresApi(Build.VERSION_CODES.O)
@Composable
private fun DateDisplay(
    event: CalendarEvent
) {
    val dateFormatter: DateTimeFormatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM, FormatStyle.SHORT)
    val startDate : LocalDateTime = LocalDateTime.ofEpochSecond(event.startTimestamp.toLong() ?: 0, 0, OffsetDateTime.now().offset)
    val endDate : LocalDateTime = LocalDateTime.ofEpochSecond(event.endTimestamp.toLong() ?: 0, 0, OffsetDateTime.now().offset)
    Row(
        horizontalArrangement = Arrangement.SpaceBetween,
        modifier = Modifier.fillMaxWidth()
    ) {
        Spacer(modifier = Modifier.width(0.5.dp))
        Text(
            text = "${stringResource(R.string.start)}: ${startDate.format(dateFormatter)}",
            fontWeight = FontWeight.Bold,
            fontSize = 13.sp
        )
        Text(
            text = "${stringResource(R.string.end)}: ${endDate.format(dateFormatter)}",
            fontWeight = FontWeight.Bold,
            fontSize = 13.sp
        )
        Spacer(modifier = Modifier.width(0.5.dp))
    }
}
