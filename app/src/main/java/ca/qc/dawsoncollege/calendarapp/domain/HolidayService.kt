package ca.qc.dawsoncollege.calendarapp.domain

import ca.qc.dawsoncollege.calendarapp.data.HolidayResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface HolidayService {
    @GET("PublicHolidays/{year}/{country}")
    suspend fun getHolidays(
        @Path("year") year: String,
        @Path("country") countryCode: String
    ) : Response<List<HolidayResponse>>
}