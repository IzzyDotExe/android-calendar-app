package ca.qc.dawsoncollege.calendarapp.domain

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.GregorianCalendar

@Entity(tableName = "events")
data class CalendarEvent (
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id") var eventId: Int = 0,
    @ColumnInfo(name = "title") var title: String = "New Meeting",
    @ColumnInfo(name = "startTimestamp") var startTimestamp: Int = (GregorianCalendar.getInstance().time.time/1000).toInt(),
    @ColumnInfo(name = "endTimestamp") var endTimestamp: Int = startTimestamp + 3600,
    @ColumnInfo(name = "description") var description: String = "",
    @ColumnInfo(name = "location") var location: String = "Dawson College"
)