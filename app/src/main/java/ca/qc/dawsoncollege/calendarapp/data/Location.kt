package ca.qc.dawsoncollege.calendarapp.data

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import ca.qc.dawsoncollege.calendarapp.domain.LatLong
import com.google.android.gms.location.LocationServices


private const val REQUEST_LOCATION_PERMISSION = 123
@SuppressLint("MissingPermission")
fun getLocation(context: Context, activity: Activity): LatLong {
    try {
        val locationFinePerms =
            ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
        val locationCoarsePerms =
            ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION)
        if (locationFinePerms == PackageManager.PERMISSION_GRANTED && locationCoarsePerms == PackageManager.PERMISSION_GRANTED) {
            return getUserLocation(context)
        } else {
            ActivityCompat.requestPermissions(
                activity, arrayOf(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ),
                REQUEST_LOCATION_PERMISSION
            )
        }
    }catch (e: Exception){
        Log.e("Location_error", "${e.message}")
    }
    return LatLong()
}
@SuppressLint("MissingPermission")
private fun getUserLocation(context: Context):LatLong{
    var currentUserLocation = LatLong()
    // The Fused Location Provider provides access to location APIs.
    var locationProvider = LocationServices.getFusedLocationProviderClient(context)


    //DisposableEffect(key1 = locationProvider) {
    //locationCallback = object : LocationCallback() {
    //override fun onLocationResult(result: LocationResult) {

    /**
     * Will return null if no historical location is available
     * */
    locationProvider.lastLocation
        .addOnSuccessListener { location ->
            location?.let {
                val lat = location.latitude
                val long = location.longitude
                currentUserLocation = LatLong(latitude = lat, longitude = long)
            }
        }
        .addOnFailureListener {
            Log.e("Location_error", "${it.message}")
        }
    return currentUserLocation
}
