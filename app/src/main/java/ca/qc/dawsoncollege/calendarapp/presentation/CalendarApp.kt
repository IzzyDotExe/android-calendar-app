package ca.qc.dawsoncollege.calendarapp.presentation

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.annotation.StringRes
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import ca.qc.dawsoncollege.calendarapp.R
import ca.qc.dawsoncollege.calendarapp.presentation.composable.CalendarEventEditView
import ca.qc.dawsoncollege.calendarapp.presentation.composable.CalendarEventView
import ca.qc.dawsoncollege.calendarapp.presentation.composable.CalendarForecastView
import ca.qc.dawsoncollege.calendarapp.presentation.composable.DayDisplay
import ca.qc.dawsoncollege.calendarapp.presentation.composable.MonthlyView
import ca.qc.dawsoncollege.calendarapp.presentation.viewmodel.CalendarViewModel
import ca.qc.dawsoncollege.calendarapp.presentation.viewmodel.HolidayViewModel

enum class CalendarAppStates(@StringRes val title: Int) {
    Monthly(title= R.string.monthly),
    Daily(title= R.string.daily),
    Forecast(title= R.string.forecast),
    Event(title= R.string.event),
    EventEdit(title= R.string.eventEdit)
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CalendarAppBar(
    currentScreen: CalendarAppStates,
    canNavBack: Boolean,
    navigateUp: () -> Unit,
    modifier: Modifier = Modifier
) {
    
    TopAppBar(title = {Text(stringResource(id = currentScreen.title))},
        colors = TopAppBarDefaults.mediumTopAppBarColors(
            containerColor = MaterialTheme.colorScheme.primaryContainer
        ),
        modifier = modifier,
        navigationIcon = {
            if (canNavBack) {
                IconButton(onClick = navigateUp) {
                    Icon(
                        imageVector = Icons.Filled.ArrowBack,
                        contentDescription = stringResource(id = R.string.back_button)
                    )
                }
            }
        }
    )
}

@RequiresApi(Build.VERSION_CODES.O)
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CalendarApp(
    viewModel: CalendarViewModel,
    holidayModel: HolidayViewModel,
    navController: NavHostController = rememberNavController()
) {

    val backStackEntry by navController.currentBackStackEntryAsState()
    val currentScreen = CalendarAppStates.valueOf(
        backStackEntry?.destination?.route?: CalendarAppStates.Monthly.name
    )
    Scaffold(
        topBar = {
            CalendarAppBar(
                currentScreen = currentScreen,
                canNavBack = navController.previousBackStackEntry != null,
                navigateUp = { navController.navigateUp() })
        },
        modifier = Modifier.fillMaxSize()
    ) { innerPadding ->
        val uiState = viewModel.uiState.collectAsState();
        val context = LocalContext.current
        NavHost(navController = navController,
            startDestination = CalendarAppStates.Monthly.name,
            modifier = Modifier.padding(innerPadding)
        ) {

            composable(route= CalendarAppStates.Event.name) {
                CalendarEventView(
                    viewModel.getCurrentEvent(),
                    viewModel = viewModel,
                    navOut = {
                        navController.popBackStack()
                    },
                    context = context,
                    onEditClick = {
                        navController.navigate(
                            CalendarAppStates.EventEdit.name
                        )
                    }
                )
            }

            composable(route= CalendarAppStates.Forecast.name) {
                CalendarForecastView(viewModel = viewModel)
            }
            composable(route= CalendarAppStates.EventEdit.name) {
                CalendarEventEditView(
                    event =  viewModel.getCurrentEvent(),
                    navBack = { navController.navigateUp() },
                    viewModel = viewModel,
                    context = context
                )
            }
            composable(route= CalendarAppStates.Monthly.name) {
                MonthlyView(viewModel, navToDaily = {
                    navController.navigate(
                        CalendarAppStates.Daily.name
                    )},
                    onCreateClick = {
                        val eventId = viewModel.newEvent()
                        viewModel.setCurrentEvent(eventId)
                        navController.navigate(

                            CalendarAppStates.EventEdit.name
                        )
                    }
                )
            }

            composable(route= CalendarAppStates.Daily.name) {
                DayDisplay(holidayModel,
                    viewModel,
                    onCreateClick = {
                        navController.navigate(
                            CalendarAppStates.EventEdit.name
                        )
                    },
                    onEventClick = { event ->
                        viewModel.setCurrentEvent(event.eventId)
                        navController.navigate(
                            CalendarAppStates.Event.name
                        )
                    },
                    onForecastClick = {
                        navController.navigate(
                            CalendarAppStates.Forecast.name
                        )
                    })
            }
        }
    }
}