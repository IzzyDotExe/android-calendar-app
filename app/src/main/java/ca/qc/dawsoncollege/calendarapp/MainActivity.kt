package ca.qc.dawsoncollege.calendarapp

import android.app.Activity
import android.app.Application
import android.os.Build
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.annotation.RequiresApi
import androidx.compose.ui.platform.LocalContext
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewmodel.compose.LocalViewModelStoreOwner
import androidx.lifecycle.viewmodel.compose.viewModel
import ca.qc.dawsoncollege.calendarapp.presentation.CalendarApp
import ca.qc.dawsoncollege.calendarapp.presentation.viewmodel.CalendarViewModel
import ca.qc.dawsoncollege.calendarapp.presentation.viewmodel.HolidayViewModel
import ca.qc.dawsoncollege.calendarapp.ui.theme.CalendarAppTheme

class MainActivity : ComponentActivity() {
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            CalendarAppTheme {
                val owner = LocalViewModelStoreOwner.current

                owner?.let {
                    val viewModel: CalendarViewModel = viewModel(
                        it,
                        "CalendarViewModel",
                        CalendarViewModelFactory(
                            LocalContext.current.applicationContext
                                    as Application
                        )
                    )
                    viewModel.fetchDataAtIntervals(this)
                    val holidayViewModel: HolidayViewModel = viewModel(
                        it,
                        "HolidayViewModel",
                        HolidayViewModelFactory(this)
                    )
                    holidayViewModel.fetchHolidays(2023, this);
                    viewModel.setHolidayViewModel(holidayViewModel)
                    CalendarApp(viewModel, holidayViewModel)
                }
            }
        }
    }

}
class CalendarViewModelFactory  (val application: Application) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return CalendarViewModel(application) as T
    }
}

class HolidayViewModelFactory (val activity: Activity) :
    ViewModelProvider.Factory{
    override fun <T: ViewModel> create(modelClass: Class<T>) : T {
        return HolidayViewModel(activity) as T
    }
}
