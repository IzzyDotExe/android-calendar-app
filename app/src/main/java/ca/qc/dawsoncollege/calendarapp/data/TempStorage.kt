package ca.qc.dawsoncollege.calendarapp.data

import android.content.Context
import android.util.Log
import java.io.IOException

class TempStorage(theContext: Context) {
    private val context = theContext
    fun writeDataToFile(dataString: String, filename: String){
        try {
            context.openFileOutput(filename, Context.MODE_PRIVATE).use {
                it.write(dataString.toByteArray())
            }
        } catch (e: IOException) {
            Log.d("fetchData", "Error writing data to file: ${e.message}")
        }
    }
    fun readDataFromFile(filename: String): String? {
        var data = ""
        try {
            data = context.openFileInput(filename).bufferedReader()
                .use { it.readText()}
        } catch (e: IOException){
            Log.e("readDataFrom file", "ERROR reading data from file ")
        }
        return data
    }
}