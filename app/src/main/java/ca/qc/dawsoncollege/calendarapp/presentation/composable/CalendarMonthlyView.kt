package ca.qc.dawsoncollege.calendarapp.presentation.composable

import android.icu.util.Calendar
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.ArrowForward
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonColors
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringArrayResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import ca.qc.dawsoncollege.calendarapp.R
import ca.qc.dawsoncollege.calendarapp.domain.CalendarEvent
import ca.qc.dawsoncollege.calendarapp.presentation.viewmodel.CalendarViewModel

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MonthlyView(
    viewModel: CalendarViewModel,
    navToDaily: () -> Unit,
    onCreateClick: () -> Unit
){
    val calUiState by viewModel.uiState.collectAsState()
    val year = calUiState.currentYear
    val month = calUiState.currentMonth

    Column(
        verticalArrangement = Arrangement.SpaceBetween,
    ) {
        MonthlyViewControls(year, month, viewModel)
        CalendarGrid(year, month, navToDaily, viewModel)
        FloatingActionButton(
            onClick = { onCreateClick() },
            Modifier.align(Alignment.End)
        ) {
            Icon(Icons.Filled.Add, stringResource(R.string.add_new_event))
        }
    }
}
@Composable
fun MonthlyViewControls(
    year: Int,
    month: Int,
    viewModel: CalendarViewModel
){
    val months: Array<String> = stringArrayResource(R.array.months)
    Row(
        horizontalArrangement = Arrangement.SpaceBetween,
        modifier = Modifier.fillMaxWidth()
    ) {
        IconButton(onClick = {
            if (month == 1) {
                viewModel.updateCurrentDate(year - 1, 12, 1)
            } else {
                viewModel.updateCurrentDate(year, month - 1, 1)
            }
        }) {
            Icon(
                imageVector = Icons.Filled.ArrowBack,
                contentDescription = stringResource(id = R.string.back_button)
            )
        }
        Text(text = "${months[month - 1]} $year")
        IconButton(onClick = {
            if (month == 12) {
                viewModel.updateCurrentDate(year + 1, 1, 1)
            } else {
                viewModel.updateCurrentDate(year, month + 1, 1)
            }
        }) {
            Icon(
                imageVector = Icons.Filled.ArrowForward,
                contentDescription = stringResource(id = R.string.back_button)
            )
        }
    }
}
@Composable
fun CalendarGrid(
    year: Int,
    month: Int,
    navToDaily: () -> Unit,
    viewModel: CalendarViewModel
){
    val cal : Calendar = Calendar.getInstance()
    // get today's date/month/year to display a different button
    val dateToday = cal.get(Calendar.DATE)
    val monthToday = cal.get(Calendar.MONTH)
    val yearToday = cal.get(Calendar.YEAR)
    // calendar instance for the given year and month
    // months are 0-indexed so -1 here
    cal.set(year, month - 1, 1)
    // list of weekdays for the calendar header, starting with Sunday
    val weekDays: Array<String> = stringArrayResource(R.array.week_days)
    val events: List<CalendarEvent> = viewModel.getEventsForCurrentMonth()
    val daysWithEvents: ArrayList<Int> = ArrayList()
    events.forEach { event ->
        val eventStartDate = viewModel.getDateFromTimestamp(event.startTimestamp)
        daysWithEvents.add(eventStartDate[java.util.Calendar.DAY_OF_MONTH])
    }
    Column {
        // header with weekdays
        Row(
            horizontalArrangement = Arrangement.SpaceEvenly,
            modifier = Modifier
                .fillMaxWidth()
                .padding(0.dp, 10.dp)
        ) {
            for (i in 1..weekDays.size) {
                val index = (i - 2 + cal.firstDayOfWeek) % weekDays.size
                val day = weekDays[index]
                Text(text = day)
            }
        }
        // buttonSize applied to Buttons and empty spaces (offset)
        val buttonSize = Modifier.size(40.dp)
        // rows for each week of the month
        for (i in 0 until 6) {
            CalendarGridRow(
                cal,
                year,
                month,
                i,
                buttonSize,
                yearToday,
                monthToday,
                dateToday,
                daysWithEvents.distinct(),
                navToDaily,
                viewModel
            )
        }
    }
}
@Composable
fun CalendarGridRow(
    cal: Calendar,
    year: Int,
    month: Int,
    i: Int,
    buttonSize: Modifier,
    yearToday: Int,
    monthToday: Int,
    dateToday: Int,
    daysWithEvent: List<Int>,
    navToDaily: () -> Unit,
    viewModel: CalendarViewModel
){
    val lastDayOfMonth = cal.getActualMaximum(Calendar.DAY_OF_MONTH)
    // index of the 1st of the month (1 to 7 = sunday to saturday)
    // used to offset first of the month in the first week
    val dayOfWeekForFirstOfMonth = cal.get(Calendar.DAY_OF_WEEK)
    Row(
        horizontalArrangement = Arrangement.SpaceEvenly,
        modifier = Modifier
            .fillMaxWidth()
            .padding(0.dp, 5.dp)
    ) {
        for (j in 0 until 7) {
            val day =  i * 7 + cal.firstDayOfWeek + j
            val offset = (dayOfWeekForFirstOfMonth - 1)
            // leave cells blank before first and after last of the month
            if (day < dayOfWeekForFirstOfMonth ||
                day > lastDayOfMonth + offset
            ) {
                Surface(modifier = buttonSize) {}
            } else {
                var colour: ButtonColors = ButtonDefaults.buttonColors()
                if (year == yearToday && month == monthToday + 1 && day - offset == dateToday) {
                    colour =
                        ButtonDefaults.buttonColors(containerColor = MaterialTheme.colorScheme.tertiary)
                } else if (daysWithEvent.contains(day - offset)){
                    colour =
                        ButtonDefaults.buttonColors(containerColor = MaterialTheme.colorScheme.outline)
                }
                Button(
                    // update viewModel and navigate to dailyView
                    onClick = {
                        viewModel.updateCurrentDate(year, month, day-offset)
                        navToDaily() },
                    modifier = buttonSize,
                    colors = colour,
                    shape = RoundedCornerShape(10.dp),
                    contentPadding = PaddingValues(0.dp),
                ) {
                    Text(text = (day - offset).toString())
                }
            }
        }
    }
}