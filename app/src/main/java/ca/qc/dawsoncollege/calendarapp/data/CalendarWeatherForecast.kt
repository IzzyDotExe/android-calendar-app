package ca.qc.dawsoncollege.calendarapp.data

import android.app.Activity
import android.content.Context
import android.util.Log
import androidx.core.os.LocaleListCompat
import java.net.HttpURLConnection
import java.net.URL

class CalendarWeatherForecast (context: Context, activity: Activity) {
    private val context = context
    private val activity = activity
    fun fetchData(tempFile: String) {
        try {
            val latLong = getLocation(context, activity)

            val currentLocale = LocaleListCompat.getDefault()[0]
            val languageCode = currentLocale?.language

            val theUrl = "https://api.weatherapi.com/v1/forecast.json?key=6e5ef4a714d845f4826194450240801&q=${latLong.latitude},${latLong.longitude}&days=5&aqi=yes&alerts=yes&lang=$languageCode"

            val url = URL(theUrl)
            val httpURLConnection = url.openConnection() as HttpURLConnection
            httpURLConnection.requestMethod = "GET"
            httpURLConnection.setRequestProperty("Accept", "application/json")
            val responseCode = httpURLConnection.responseCode

            if (responseCode == HttpURLConnection.HTTP_OK) {
                val dataString = httpURLConnection.inputStream.bufferedReader()
                    .use { it.readText() }
                TempStorage(context).writeDataToFile(dataString, tempFile)
            } else {
                Log.e("httpsURLConnection_ERROR", responseCode.toString())
            }
        }catch (e: Exception){
            Log.e("httpsURLConnection_ERROR", "error fetching data", e)
        }
    }
}
