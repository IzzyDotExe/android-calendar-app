package ca.qc.dawsoncollege.calendarapp.data

import android.app.Activity
import android.content.Context
import android.content.res.Resources
import android.location.Geocoder
import android.telephony.TelephonyManager
import ca.qc.dawsoncollege.calendarapp.domain.HolidayService
import ca.qc.dawsoncollege.calendarapp.domain.Location
import ca.qc.dawsoncollege.calendarapp.domain.RetrofitInstance
import com.google.android.gms.location.LocationServices
import java.util.Locale

class HolidayRepository {

    private val holidayService: HolidayService = RetrofitInstance.getApi()

    suspend fun getHolidays(year: Int, locale: String): List<HolidayResponse> {
        return holidayService.getHolidays(year.toString(), locale).body() ?: listOf()
    }

    fun getLocaleCode(context: Context, activity: Activity): String {
        var code = Geocoder(context)
        var latlong = getLocation(context, activity)
        var addrs = code.getFromLocation(latlong.latitude, latlong.longitude, 1);
        if (addrs?.size!! > 0) {
            return addrs[0]?.countryCode.toString();
        }
        return "CA"
    }

}