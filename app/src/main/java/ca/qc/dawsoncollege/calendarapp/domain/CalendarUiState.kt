package ca.qc.dawsoncollege.calendarapp.domain

data class CalendarUiState(

    val currentMonth: Int = 12,

    val currentYear: Int = 2023,

    val currentDay: Int = 8,

    val currentEvent: Int = 0,

    val events: MutableList<CalendarEvent> = mutableListOf(
        CalendarEvent(1, "e1", 1702018800, 1702022400, "",),
        CalendarEvent(2, "e2", 1702040400, 1702047600, "desc"),
        CalendarEvent(3, "e3", 1702076400, 1702094400, "desc")
    )
)