package ca.qc.dawsoncollege.calendarapp.presentation.composable

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.unit.times
import ca.qc.dawsoncollege.calendarapp.domain.CalendarEvent
import ca.qc.dawsoncollege.calendarapp.presentation.viewmodel.CalendarViewModel
import ca.qc.dawsoncollege.calendarapp.presentation.viewmodel.HolidayViewModel
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import java.util.Calendar
import java.util.Locale


private fun getDayOfWeek(timestamp: Int): String? {
    val calendar = Calendar.getInstance()
    calendar.timeInMillis = timestamp * 1000L // Convert seconds to milliseconds
    val dateFormat = SimpleDateFormat("EEEE")
    return dateFormat.format(calendar.time)
}

var totalEventVerticalOffset = 0.dp

var currentDay = mutableStateOf(0)
    @RequiresApi(Build.VERSION_CODES.O)
    @Composable
    fun DayDisplay(
        holidayViewModel: HolidayViewModel,
        calViewModel: CalendarViewModel,
        onCreateClick: () -> Unit,
        onEventClick: (CalendarEvent) -> Unit,
        onForecastClick: () -> Unit)
    {
        val uiState = calViewModel.uiState.collectAsState()
        currentDay.value = uiState.value.currentDay
        var events = calViewModel.getEventsStartingOnCurrentDay()

        // Add more events as needed
        Column (
            modifier = Modifier
                .padding(16.dp),
        ){
            Row (modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween){
                Button(onClick = { calViewModel.previousDay() },
                    modifier = Modifier.background(MaterialTheme.colorScheme.background)) {
                    Text(text = "<")
                }
                Text(text = "${getDayString(calViewModel)}")
                Button(onClick = { calViewModel.nextDay()},
                    modifier = Modifier.background(MaterialTheme.colorScheme.background)) {
                    Text(text = ">")
                }
            }

            displayWeather(calViewModel, onForecastClick)

            LazyRow(modifier = Modifier
                .height(50.dp)
                .fillMaxWidth()){
                item {
                    holidayViewModel.holidays.value?.filter {
                        val eventStartDate = calViewModel.getDateFromTimestamp(it.startTimestamp)
                        eventStartDate[Calendar.DAY_OF_MONTH] == calViewModel.uiState.value.currentDay &&
                                eventStartDate[Calendar.MONTH] == calViewModel.uiState.value.currentMonth-1 &&
                                eventStartDate[Calendar.YEAR] == calViewModel.uiState.value.currentYear
                    }?.forEach {
                        EventItem(viewModel = calViewModel, event = it, navEvent = {

                        })
                    }
                }
            }

            LazyColumn(
                modifier = Modifier
                    .fillMaxHeight()
                    .fillMaxWidth()
                    .background(MaterialTheme.colorScheme.background)
            ) {
                item {
                    Row(modifier = Modifier.fillMaxWidth(),
                        horizontalArrangement = Arrangement.End) {
                        Button(onClick = {
                            val eventId = calViewModel.newEvent()
                            calViewModel.setCurrentEvent(eventId)
                            onCreateClick()
                        }) {
                            Text(text = "+")
                        }
                    }
                    Row {
                        TimeSlots()
                        Column(
                            modifier = Modifier
                                .fillMaxHeight()
                                .fillMaxWidth()
                                .width(100.dp)
                        ) {
                            // Content of the second column
                            totalEventVerticalOffset = 0.dp
                            val sortedEvents = events.sortedBy { it.startTimestamp }
                            sortedEvents.forEach { event ->
                                EventItem(calViewModel, event, onEventClick)
                            }
                        }
                    }
                }
            }
        }
    }

@Composable
fun displayWeather(viewModel: CalendarViewModel, onClickFunc: () -> Unit) {
    val calendar = Calendar.getInstance()
    calendar.set(Calendar.HOUR_OF_DAY, 0)
    calendar.set(Calendar.MINUTE, 0)
    calendar.set(Calendar.SECOND, 0)
    calendar.set(Calendar.MILLISECOND, 0)
    val systemEpoch = calendar.timeInMillis / 1000
    val uiState = viewModel.uiState.collectAsState()
    val weatherData = viewModel.forecastDataState.collectAsState()
    val currentEpoch = viewModel.getEpoch(
        uiState.value.currentYear,
        uiState.value.currentMonth-1,
        uiState.value.currentDay,
        0,0,0
    )
    if (
        currentEpoch in systemEpoch ..(systemEpoch+4*24*3600)
    ) {
        val day = ((currentEpoch-systemEpoch+1)/86400).toInt()
        val currentDayData = weatherData.value.forecast.forecastday[day]
        Row(modifier = Modifier
            .fillMaxWidth()
            .clickable { onClickFunc() }) {
            Column {
                Text(text = "${weatherData.value.current.lastUpdated}")
                Text(text = "${currentDayData.day.avgTempC}°C")
                Text(text = "wind ${currentDayData.day.maxWindKph}km/h")
                Text(text = "${currentDayData.day.condition.text}")
            }
        }
    }
}

fun getMonth(timestamp: Long): String {
    val calendar = Calendar.getInstance()
    calendar.timeInMillis = timestamp * 1000 // Convert seconds to milliseconds
    val month = calendar.get(Calendar.MONTH)

    // Assuming you want the month name as a string
    return SimpleDateFormat("MMMM", Locale.getDefault()).format(calendar.time)
}

@RequiresApi(Build.VERSION_CODES.O)
fun getDayString(viewModel: CalendarViewModel): String? {
    val dateFormatter: DateTimeFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL)
    val uiState = viewModel.uiState.value
    val date : LocalDate = LocalDate.of(uiState.currentYear, uiState.currentMonth, uiState.currentDay)
    return date.format(dateFormatter)
}

@Composable
    fun TimeSlots() {
        Column{
            val hours = (0..23).toList()
            hours.forEach { hour ->
                Row(
                    modifier = Modifier
                        .height(50.dp)
                        .width(100.dp)
                ) {
                    Text(
                        text = "$hour:00",
                        fontSize = 16.sp,
                        modifier = Modifier
                            .padding(8.dp)
                    )
                }
            }
        }

    }

@Composable
fun EventItem(viewModel: CalendarViewModel, event: CalendarEvent, navEvent: (CalendarEvent) -> Unit) {
    var startTime = getHourFromTimestamp(viewModel, event.startTimestamp)
    var endTime = getHourFromTimestamp(viewModel, event.endTimestamp)
    var startY = ((startTime) * 50.dp) - totalEventVerticalOffset
    var height = (endTime - startTime) * 50.dp
    if (startY < 0.dp)
    {
        startY = 0.dp
    }

    totalEventVerticalOffset += height + startY
    Box(
        modifier = Modifier.clickable {
            viewModel.setCurrentEvent(eventId = event.eventId)
            navEvent(event)
        },
    ) {
        Text(
            color = MaterialTheme.colorScheme.onBackground,
            text = "${event.title}\n ${event.description}",
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = startY)
                .clip(RoundedCornerShape(16.dp))
                .height(height)
                .background(MaterialTheme.colorScheme.primaryContainer)
                .padding(8.dp),
        )
    }
}

fun getHourFromTimestamp(viewModel: CalendarViewModel, timestamp: Int): Int {
    val calendar = viewModel.getDateFromTimestamp(timestamp)
    return calendar[Calendar.HOUR_OF_DAY]
}