package ca.qc.dawsoncollege.calendarapp.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import ca.qc.dawsoncollege.calendarapp.domain.CalendarEvent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

class CalendarEventRepository(private val eventDao: CalendarEventDao) {

    val allEvents: LiveData<List<CalendarEvent>> = eventDao.getAllEvents()

    val searchResults = MutableLiveData<List<CalendarEvent>>()

    private val coroutineScope = CoroutineScope(Dispatchers.Main)


    fun insertEvent(newEvent: CalendarEvent) {
        coroutineScope.launch(Dispatchers.IO) {
            eventDao.insertEvent(newEvent)
        }
    }

    fun deleteEvent(id: String) {
        coroutineScope.launch(Dispatchers.IO) {
            eventDao.deleteEvent(id)
        }
    }

    fun updateEvent(event: CalendarEvent) {
        coroutineScope.launch(Dispatchers.IO) {
            eventDao.updateEvent(event)
        }
    }

    fun findEvent(id: String) {
        coroutineScope.launch(Dispatchers.Main) {
            searchResults.value = asyncFind(id).await()
        }
    }

    private fun asyncFind(name: String): Deferred<List<CalendarEvent>?> =
        coroutineScope.async(Dispatchers.IO) {
            return@async eventDao.findEvent(name)
        }
}