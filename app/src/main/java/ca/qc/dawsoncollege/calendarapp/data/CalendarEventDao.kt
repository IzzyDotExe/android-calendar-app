package ca.qc.dawsoncollege.calendarapp.data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import ca.qc.dawsoncollege.calendarapp.domain.CalendarEvent


@Dao
interface CalendarEventDao {
    @Insert
    fun insertEvent(event: CalendarEvent)

    @Update
    fun updateEvent(event: CalendarEvent)

    @Query("SELECT * FROM events WHERE id = :id")
    fun findEvent(id: String): List<CalendarEvent>
    @Query("DELETE FROM events WHERE id = :id")
    fun deleteEvent(id: String)
    @Query("SELECT * FROM events")
    fun getAllEvents(): LiveData<List<CalendarEvent>>
}