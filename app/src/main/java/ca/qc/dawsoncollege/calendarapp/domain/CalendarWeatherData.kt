package ca.qc.dawsoncollege.calendarapp.domain

import com.google.gson.annotations.SerializedName
import kotlinx.serialization.Serializable

@Serializable
data class WeatherData(
    val location: Location = Location(),
    val current: Current = Current(),
    val forecast: Forecast = Forecast()
)
//TODO remove unwanted fields
@Serializable
data class Location(
    val name: String = "",
    val region: String = "",
    val country: String = "",
    val lat: Double = 0.0,
    val lon: Double = 0.0,
    val tz_id: String = "",
    @SerializedName("localtime_epoch")
    val localTimeEpoch:  Long = 0,
    val localtime: String = ""
)

@Serializable
data class Current(
    @SerializedName("last_updated")
    val lastUpdated: String = "",
    @SerializedName("temp_c")
    val tempC: Double = 0.0,
    @SerializedName("temp_f")
    val tempF: Double = 0.0,
    @SerializedName("is_day")
    val isDay: Int = 0,
    val condition: Condition = Condition(),
    @SerializedName("wind_mph")
    val windMph: Double = 0.0,
    @SerializedName("wind_kph")
    val windKph: Double = 0.0,
    @SerializedName("wind_degree")
    val windDegree: Int = 0,
    @SerializedName("wind_dir")
    val windDir: String = "",
    @SerializedName("pressure_mb")
    val pressureMb: Double = 0.0,
    @SerializedName("pressure_in")
    val pressureIn: Double = 0.0,
    @SerializedName("precip_mm")
    val precipMm: Double = 0.0,
    @SerializedName("precip_in")
    val precipIn: Double = 0.0,
    val humidity: Int = 0,
    val cloud: Int = 0,
    @SerializedName("feelslike_c")
    val feelslikeC: Double = 0.0,
    @SerializedName("feelslike_f")
    val feelslikeF: Double = 0.0,
    @SerializedName("vis_km")
    val visKm: Double = 0.0,
    @SerializedName("vis_miles")
    val visMiles: Double = 0.0,
    val uv: Double = 0.0,
    @SerializedName("gust_mph")
    val gustMph: Double = 0.0,
    @SerializedName("gust_kph")
    val gustKph: Double = 0.0
)

@Serializable
data class Condition(
    val text: String = "",
    val icon: String = "",
    val code: Int = 0
)

@Serializable
data class Forecast(
    val forecastday: List<ForecastDay> = emptyList()
)

@Serializable
data class ForecastDay(
    val date: String = "",
    @SerializedName("date_epoch")
    val dateEpoch:  Long = 0,
    val day: Day,
    val hour: List<Hour>
)

@Serializable
data class Hour(
    @SerializedName("time_epoch")
    val timeEpoch:  Long = 0,
    val time: String = "",
    @SerializedName("temp_c")
    val tempC: Double = 0.0,
    @SerializedName("temp_f")
    val tempF: Double = 0.0,
    @SerializedName("is_day")
    val isDay: Int = 0,
    val condition: Condition = Condition(),
    @SerializedName("wind_mph")
    val windMph: Double = 0.0,
    @SerializedName("wind_kph")
    val windKph: Double = 0.0,
    @SerializedName("wind_degree")
    val windDegree: Int = 0,
    @SerializedName("wind_dir")
    val windDir: String = "",
    @SerializedName("pressure_mb")
    val pressureMb: Double = 0.0,
    @SerializedName("pressure_in")
    val pressureIn: Double = 0.0,
    @SerializedName("precip_mm")
    val precipMm: Double = 0.0,
    @SerializedName("precip_in")
    val precipIn: Double = 0.0,
    @SerializedName("snow_cm")
    val snowCm: Double = 0.0,
    val humidity: Int = 0,
    val cloud: Int = 0,
    @SerializedName("feelslike_c")
    val feelslikeC: Double = 0.0,
    @SerializedName("feelslike_f")
    val feelslikeF: Double = 0.0,
    @SerializedName("windchill_c")
    val windchillC: Double = 0.0,
    @SerializedName("windchill_f")
    val windchillF: Double = 0.0,
    @SerializedName("heatindex_c")
    val heatindexC: Double = 0.0,
    @SerializedName("heatindex_f")
    val heatindexF: Double = 0.0,
    @SerializedName("dewpoint_c")
    val dewpointC: Double = 0.0,
    @SerializedName("dewpoint_f")
    val dewpointF: Double = 0.0,
    @SerializedName("will_it_rain")
    val willItRain: Int = 0,
    @SerializedName("chance_of_rain")
    val chanceOfRain: Int = 0,
    @SerializedName("will_it_snow")
    val willItSnow: Int = 0,
    @SerializedName("chance_of_snow")
    val chanceOfSnow: Int = 0,
    @SerializedName("vis_km")
    val visKm: Double = 0.0,
    @SerializedName("vis_miles")
    val visMiles: Double = 0.0,
    @SerializedName("gust_mph")
    val gustMph: Double = 0.0,
    @SerializedName("gust_kph")
    val gustKph: Double = 0.0,
    val uv: Double = 0.0
)

@Serializable
data class Day(
    @SerializedName("maxtemp_c")
    val maxTempC: Double = 0.0,
    @SerializedName("maxtemp_f")
    val maxTempF: Double = 0.0,
    @SerializedName("mintemp_c")
    val minTempC: Double = 0.0,
    @SerializedName("mintemp_f")
    val minTempF: Double = 0.0,
    @SerializedName("avgtemp_c")
    val avgTempC: Double = 0.0,
    @SerializedName("avgtemp_f")
    val avgTempF: Double = 0.0,
    @SerializedName("maxwind_mph")
    val maxWindMph: Double = 0.0,
    @SerializedName("maxwind_kph")
    val maxWindKph: Double = 0.0,
    @SerializedName("totalprecip_mm")
    val totalPrecipMm: Double = 0.0,
    @SerializedName("totalprecip_in")
    val totalPrecipIn: Double = 0.0,
    @SerializedName("totalsnow_cm")
    val totalSnowCm: Double = 0.0,
    @SerializedName("avgvis_km")
    val avgVisKm: Double = 0.0,
    @SerializedName("avgvis_miles")
    val avgVisMiles: Double = 0.0,
    @SerializedName("avghumidity")
    val avgHumidity: Double = 0.0,
    @SerializedName("daily_will_it_rain")
    val dailyWillItRain: Int = 0,
    @SerializedName("daily_chance_of_rain")
    val dailyChanceOfRain: Int = 0,
    @SerializedName("daily_will_it_snow")
    val dailyWillItSnow: Int = 0,
    @SerializedName("daily_chance_of_snow")
    val dailyChanceOfSnow: Int = 0,
    val condition: Condition = Condition(),
    val uv: Double = 0.0
)

@Serializable
data class AirQuality(
    val co: Double = 0.0,
    val no2: Double = 0.0,
    val o3: Double = 0.0,
    val so2: Double = 0.0,
    @SerializedName("pm2_5")
    val pm25: Double = 0.0,
    @SerializedName("pm10")
    val pm10: Double = 0.0,
    @SerializedName("us_epa_index")
    val usEpaIndex: Int = 0,
    @SerializedName("gb_defra_index")
    val gbDefraIndex: Int = 0
)

