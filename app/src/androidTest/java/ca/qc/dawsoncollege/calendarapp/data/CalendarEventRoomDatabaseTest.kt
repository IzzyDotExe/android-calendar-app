package ca.qc.dawsoncollege.calendarapp.data

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import ca.qc.dawsoncollege.calendarapp.domain.CalendarEvent
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertFalse
import junit.framework.TestCase.assertTrue
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException

@RunWith(AndroidJUnit4::class)
class CalendarEventRoomDatabaseTest {

    private lateinit var db: CalendarEventRoomDatabase
    private lateinit var dao: CalendarEventDao

    @Before
    fun setUp() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(context, CalendarEventRoomDatabase::class.java).build()
        dao = db.calendarEventDao()

    }

    @After
    fun closeDb() {
        db.close()
    }

    @Test
    fun writeReadEvent() = runTest{
        val event = CalendarEvent(1, "title", 1230, 23432, "description", "location")
        val event1 = CalendarEvent(2, "title1", 123034, 2342342, "description1", "location1")
        dao.insertEvent(event)
        dao.insertEvent(event1)
        val events = dao.getAllEvents().getOrAwaitValue()
        assertTrue(events.contains(event))
        assertTrue(events.contains(event1))
    }

    @Test
    fun update() = runBlocking {
        val event = CalendarEvent(1, "title", 1230, 23432, "description", "location")
        val event1 = CalendarEvent(1, "ti", 123034, 2342342, "description1", "location1")
        dao.insertEvent(event)
        val events = dao.getAllEvents()

        assertFalse(events.equals(event))
    }

    @Test
    fun delete() {
        val event = CalendarEvent(1, "title", 1230, 23432, "description", "location")
        val event1 = CalendarEvent(2, "title1", 123034, 2342342, "description1", "location1")
        dao.insertEvent(event)
        dao.insertEvent(event1)
        dao.deleteEvent("1")
        val events = dao.getAllEvents()
        assertFalse(events.value!!.contains(event))
        assertTrue(events.value!!.contains(event1))
    }

    @Test
    fun findEvent() {
        val event = CalendarEvent(1, "title", 1230, 23432, "description", "location")
        val event1 = CalendarEvent(2, "title1", 123034, 2342342, "description1", "location1")
        dao.insertEvent(event)
        dao.insertEvent(event1)
        val events = dao.findEvent("1")
        assertEquals(event,events)
    }
}

fun <T> LiveData<T>.getOrAwaitValue(
    time: Long = 2,
    timeUnit: TimeUnit = TimeUnit.SECONDS,
    afterObserve: () -> Unit = {}
): T {
    var data: T? = null
    val latch = CountDownLatch(1)
    val observer = object : Observer<T> {
        override fun onChanged(value: T) {
            data = value
            latch.countDown()
            this@getOrAwaitValue.removeObserver(this)
        }
    }
    this.observeForever(observer)

    afterObserve.invoke()

    // Don't wait indefinitely if the LiveData is not set.
    if (!latch.await(time, timeUnit)) {
        this.removeObserver(observer)
        throw TimeoutException("LiveData value was never set.")
    }

    @Suppress("UNCHECKED_CAST")
    return data as T
}