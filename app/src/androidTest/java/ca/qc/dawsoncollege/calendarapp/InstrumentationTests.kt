package ca.qc.dawsoncollege.calendarapp

import android.app.Application
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithText
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import ca.qc.dawsoncollege.calendarapp.presentation.CalendarViewModel
import ca.qc.dawsoncollege.calendarapp.presentation.composable.CalendarEventEditView
import ca.qc.dawsoncollege.calendarapp.presentation.composable.CalendarEventView
import ca.qc.dawsoncollege.calendarapp.presentation.composable.DayDisplay
import ca.qc.dawsoncollege.calendarapp.presentation.composable.MonthlyView
import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.OffsetDateTime
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class InstrumentationTests {
    @get:Rule // <----
    var instantExecutorRule = InstantTaskExecutorRule() // <----
    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        assertEquals("ca.qc.dawsoncollege.calendarapp", appContext.packageName)
    }

    @get:Rule
    val testingRule = createComposeRule()

    @Test
    fun eventScreenCorrect() {
        val application = InstrumentationRegistry.getInstrumentation().targetContext.applicationContext as Application
        val vm = CalendarViewModel(application)
        val id = vm.newEvent()
        vm.setCurrentEvent(id)
        val event = vm.getCurrentEvent()

        val sdf = SimpleDateFormat("yyyy-MM-dd @hh:mm")
        val dateFormatter: DateTimeFormatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM, FormatStyle.SHORT)
        val startDate : LocalDateTime = LocalDateTime.ofEpochSecond(event.startTimestamp.toLong() ?: 0, 0, OffsetDateTime.now().offset)
        val endDate : LocalDateTime = LocalDateTime.ofEpochSecond(event.endTimestamp.toLong() ?: 0, 0, OffsetDateTime.now().offset)
        val expectedTimeString = startDate.format(dateFormatter)
        val expectedEndTimeString = endDate.format(dateFormatter)

        testingRule.setContent {
            CalendarEventView(
                event = event,
                navOut = { /*TODO*/ },
                viewModel = vm,
                context = LocalContext.current,
                onEditClick = {}
            )
        }

        // render
        Thread.sleep(2500)

        // Test strings
        testingRule.onNodeWithText(event.title).assertExists()
        testingRule.onNodeWithText(event.description).assertExists()
        testingRule.onNodeWithText("Start: $expectedTimeString").assertExists()
        testingRule.onNodeWithText("End: $expectedEndTimeString").assertExists()

        // Buttons exist
        testingRule.onNodeWithText("Edit").assertExists()
        testingRule.onNodeWithText("Directions").assertExists()

    }

    @Test
    fun eventEditScreenCorrect() {
        val application = InstrumentationRegistry.getInstrumentation().targetContext.applicationContext as Application
        val vm = CalendarViewModel(application)
        vm.setCurrentEvent(2)

        testingRule.setContent {
            CalendarEventEditView(
                event = vm.getCurrentEvent(),
                navBack = { /*TODO*/ },
                viewModel = vm,
                context = LocalContext.current
            )
        }

        // render
        Thread.sleep(3000)

        testingRule.onNodeWithText("Start").assertExists()
        testingRule.onNodeWithText("End").assertExists()
        testingRule.onNodeWithText("Save").assertExists()

    }
    @Test
    fun dayViewScreenCorrect() {
        val application = InstrumentationRegistry.getInstrumentation().targetContext.applicationContext as Application
        val vm = CalendarViewModel(application)
        vm.updateCurrentDate(2023, 12, 8)
        testingRule.setContent {
            DayDisplay(vm, {}, {})
        }

        // render
        Thread.sleep(3000)

        testingRule.onNodeWithText("<").assertExists()
        testingRule.onNodeWithText(">").assertExists()
        testingRule.onNodeWithText("+").assertExists()
        testingRule.onNodeWithText("Friday, December 8, 2023").assertExists()
        //testingRule.onNodeWithText("e1\n ").assertExists()
        testingRule.onNodeWithText("1:00").assertExists()
        testingRule.onNodeWithText("10:00").assertExists()
    }
    @Test
    fun monthlyViewScreenCorrect() {
        val application = InstrumentationRegistry.getInstrumentation().targetContext.applicationContext as Application
        val vm = CalendarViewModel(application)
        vm.updateCurrentDate(2023, 11, 1)

        testingRule.setContent {
            MonthlyView(
                vm, {}, {}
            )
        }

        // render
        Thread.sleep(3000)

        testingRule.onNodeWithText("November 2023").assertExists()
        testingRule.onNodeWithText("Sun").assertExists()
        testingRule.onNodeWithText("Mon").assertExists()
        testingRule.onNodeWithText("Tue").assertExists()
        testingRule.onNodeWithText("Wed").assertExists()
        testingRule.onNodeWithText("Thu").assertExists()
        testingRule.onNodeWithText("Fri").assertExists()
        testingRule.onNodeWithText("Sat").assertExists()
        testingRule.onNodeWithText("1").assertExists()
        testingRule.onNodeWithText("30").assertExists()
        testingRule.onNodeWithText("31").assertDoesNotExist()
    }
}