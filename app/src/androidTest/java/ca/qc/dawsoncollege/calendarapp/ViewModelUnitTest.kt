package ca.qc.dawsoncollege.calendarapp

import android.app.Application
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.platform.app.InstrumentationRegistry
import ca.qc.dawsoncollege.calendarapp.presentation.viewmodel.CalendarViewModel
import org.junit.Assert.assertArrayEquals
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class ViewModelUnitTest {
    @get:Rule // <----
    var instantExecutorRule = InstantTaskExecutorRule() // <----

    @Test
    fun getDaysInMonthTest() {
        val application = InstrumentationRegistry.getInstrumentation().targetContext.applicationContext as Application
        val model = CalendarViewModel(application)
        val expected = listOf<Int>(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31)
        var result = mutableListOf<Int>()
        for (i in 1..12) {
            result.add(model.getDaysInMonth(i))
        }
        assertArrayEquals(expected.toIntArray(), result.toIntArray())
    }

    @Test
    fun editCurrentEventTest() {
        val application = InstrumentationRegistry.getInstrumentation().targetContext.applicationContext as Application
        val model = CalendarViewModel(application)
        val id = model.newEvent()
        model.setCurrentEvent(id)
        assertEquals("New Meeting", model.getCurrentEvent().title)
    }

    @Test
    fun getEpochTest() {
        val application = InstrumentationRegistry.getInstrumentation().targetContext.applicationContext as Application
        val model = CalendarViewModel(application)
        val expectedEpoch = 1699975198
        val testEpoch = model.getEpoch(2023, 10, 14, 10, 19, 58)
        assertEquals(expectedEpoch, testEpoch)
    }
}
